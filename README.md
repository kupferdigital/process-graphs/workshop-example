# Workshop Example

Graph created at the Handson Session in the Industry Workshop of the Copper Digital Project.

<img src="https://kupferdigital.gitlab.io/process-graphs/workshop-example/workshop-example.svg">

['ttl.file'](https://kupferdigital.gitlab.io/process-graphs/workshop-example/index.ttl)
